<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Exam;

class ExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $exams = Exam::paginate(15);
        return view('exam/index', ['exams' => $exams]);
    }
    public function destroy($id)
    {
        $exam = exam::find($id);
        $this->authorize('delete', $exam);
        $exam->delete();
        return redirect('/exams');
    }
    public function remember($id)
    {
        $exam = exam::find($id);
        session(['examremembered' => $exam]);
        return redirect('/exams');
    }
    public function forget(){
        session()->forget('examremembered');
        return redirect('/exams');

    }
}
