<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam;

class ExamController extends Controller
{
    public function show($id)
    {
        $exam = Exam::with('module')->find($id);
        if ($exam) {
            return $exam;
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }
    public function destroy($id)
    {
        $exam = Exam::find($id);
        if ($exam) {
            $exam->delete();
            return ['delete' => $id];
        }else{
            return response()->json(['message' => 'Record not found'], 404);
        }
    }

}
