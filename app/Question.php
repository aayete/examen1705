<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'text', 'a', 'b', 'c', 'd', 'answer', 'module_id'
    ];
    public function users()
    {
        return $this->belongsToMany('App\Exam', 'exam_users')
          ->withTimestamps();
    }
}
