<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'code', 'name'
    ];
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
}
