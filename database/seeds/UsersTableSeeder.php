<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'id' => 1,
            'name' => 'pepe',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')
        ->insert([
            'id' => 2,
            'name' => 'juan',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')
        ->insert([
            'id' => 3,
            'name' => 'ana',
            'email' => 'ana@gmail.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')
        ->insert([
            'id' => 4,
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
