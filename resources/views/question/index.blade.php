@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Cuestiones</h1>

    <table class="table table-bordered">
    <tr>
        <th>Pregunta</th>
        <th>Opcion A</th>
        <th>Opcion B</th>
        <th>Opcion C</th>
        <th>Opcion D</th>
        <th>Respuesta Correcta</th>
    </tr>
    @foreach ($questions as $question)
    <tr>
        <td>{{ $question->text }}</td>
        @if ($question->answer === "a")
            <td class="bg-success">{{ $question->a }}</td>
        @else
            <td>{{ $question->a }}</td>
        @endif

        @if ($question->answer === "b")
            <td class="bg-success">{{ $question->b }}</td>
        @else
            <td>{{ $question->b }}</td>
        @endif

        @if ($question->answer === "c")
            <td class="bg-success">{{ $question->c }}</td>
        @else
            <td>{{ $question->c }}</td>
        @endif

        @if ($question->answer === "d")
            <td class="bg-success">{{ $question->d }}</td>
        @else
            <td>{{ $question->d }}</td>
        @endif
        <td>{{ $question->answer }}</td>
    </tr>
    @endforeach
</table>1
{{ $questions->links() }}

<a href="/questions/create" class="btn btn-info">Nueva pregunta</a>
</div>
@endsection
