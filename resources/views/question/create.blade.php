@extends('layouts.app')

@section('content')
<div class="container">

<h1>Crear Libro</h1>

<form method="post" action="/questions">
    {{ csrf_field() }}

    <div  class="form-group">
        <label>Pregunta</label>
        <input class="form-control"  type="text" name="text" value="{{ old('text') }}">
        @if ($errors->first('text'))
            <div class="alert alert-danger">
                {{ $errors->first('text') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Opcion A</label>
        <input class="form-control"  type="text" name="a" value="{{ old('a') }}">
        @if ($errors->first('a'))
            <div class="alert alert-danger">
                {{ $errors->first('a') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Opcion B</label>
        <input class="form-control"  type="text" name="b" value="{{ old('b') }}">
        @if ($errors->first('b'))
            <div class="alert alert-danger">
                {{ $errors->first('b') }}
            </div>
        @endif
    </div>
    <div  class="form-group">
        <label>Opcion C</label>
        <input class="form-control"  type="text" name="c" value="{{ old('c') }}">
        @if ($errors->first('c'))
            <div class="alert alert-danger">
                {{ $errors->first('c') }}
            </div>
        @endif
    </div>
    <div  class="form-group">
        <label>Opcion D</label>
        <input class="form-control"  type="text" name="d" value="{{ old('d') }}">
        @if ($errors->first('d'))
            <div class="alert alert-danger">
                {{ $errors->first('d') }}
            </div>
        @endif
    </div>
    <div  class="form-group">
        <label>Respuesta</label>
        <input class="form-control"  type="text" name="answer" value="{{ old('answer') }}">
        @if ($errors->first('answer'))
            <div class="alert alert-danger">
                {{ $errors->first('answer') }}
            </div>
        @endif
    </div>

    <div  class="form-group">
        <label>Modulo</label>
        <select class="form-control" name="module_id">
            @foreach ($modules as $module)
                <option value="{{$module->id}}">{{$module->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>



</form>

</div>
@endsection
