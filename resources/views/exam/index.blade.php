@extends('layouts.app')

@section('content')
<div class="container">

<h1>Lista de Examenes</h1>

    @if (session()->get('examremembered'))
        <span >{{ Session::get('examremembered')->title }}</span>
        <a href="/exams/forget " class="btn btn-info" type="submit">Olvidar</a>
    @endif

    <table class="table table-bordered">
    <tr>
        <th>Title</th>
        <th>date</th>
        <th>Materia</th>
        <th>Persona</th>
        <th>Opciones</th>
    </tr>
    @foreach ($exams as $exam)
    <tr>
        <td>{{ $exam->title }}</td>
        <td>{{ $exam->date }}</td>
        <td>{{ $exam->user->name }}</td>
        <td>{{ $exam->module->name }}</td>
        <td>
            <form method="post" action="/exams/{{ $exam->id }}">
                @can ('delete', $exam)
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="delete">
                    <input class="btn btn-danger" type="submit" value="borrar">
                @endcan
            </form>
            <a href="/exams/{{ $exam->id }}/remember " class="btn btn-info" type="submit">Recordar</a>
        </td>

        {{-- <td>
            <a href="/spaces/{{ $space->id }}">Recordar</a>
            <form method="post" action="/spaces/{{ $space->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
        </td> --}}
    </tr>
    @endforeach
</table>

{{ $exams->links() }}
</div>
@endsection
